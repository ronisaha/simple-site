<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require_once "klein.php";

$base = '/vhosts/l2n/simple-site/index.php';

respond(
    "{$base}/vhosts/l2n/simple-site/",
    function ($request, $response) {
        echo "Home Page";
        die;
    }
);

respond(
    "{$base}/news/[*:title][i:id]",
    function ($request, $response) {
        echo "news details page : " . $request->id . " TITLE: " . $request->title;
        die;
    }
);

respond(
    "{$base}/category/[i:id]",
    function ($request, $response) {
        echo "category page: " . $request->id;
        die;
    }
);

respond(
    "*",
    function ($request, $response) {
        echo "Not found";
    }
);

dispatch();